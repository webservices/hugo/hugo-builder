kind: Template
apiVersion: v1
metadata:
  name: hugo-cern
  annotations:
    description: "HUGO for CERN template"
    # Prevent end users from binding against services provisioned by this template
    # See: https://docs.okd.io/latest/dev_guide/templates.html#writing-marking-templates-as-bindable
    template.openshift.io/bindable: "false"
labels:
  template: "hugo-cern" #this label will applied to all objects created from this template
objects:
  - kind: Service
    apiVersion: v1
    metadata:
      labels:
        app: hugo-cern
      name: hugo-svc
    spec:
      ports:
      - port: 1313
        protocol: TCP
        targetPort: 1313
      selector:
        deploymentconfig: hugo-builder
      sessionAffinity: None
      type: ClusterIP

  - kind: Route
    apiVersion: route.openshift.io/v1
    metadata:
      annotations:
        cern.ch/generated.host.replaced: "true"
      labels:
        app: hugo-cern
      name: hugo-route
    spec:
      host: ${BASE_URL}
      tls:
        insecureEdgeTerminationPolicy: Redirect
        termination: edge
      to:
        kind: Service
        name: hugo-svc
        weight: 100
      wildcardPolicy: None

  - kind: BuildConfig
    apiVersion: v1
    metadata:
      labels:
        app: hugo-cern
      name: hugo-builder
      annotations:
        description: Defines how to build the application
        # wait-for-ready used on BuildConfig ensures that template instantiation
        # will fail immediately if build fails
        template.alpha.openshift.io/wait-for-ready: "true"
    spec:
      completionDeadlineSeconds: 1200
      successfulBuildsHistoryLimit: 2
      failedBuildsHistoryLimit: 5
      output:
        to:
          kind: ImageStreamTag
          name: hugo-builder:latest
      resources:
        limits:
          cpu: 500m
          memory: 1Gi
        requests:
          cpu: 250m
          memory: 250Mi
      runPolicy: SerialLatestOnly
      source:
        type: Git
        git:
          uri: "${SOURCE_REPOSITORY_URL}" 
          ref: "${SOURCE_REPOSITORY_REF}"
      strategy:
        type: Source
        sourceStrategy:
          from:
            kind: ImageStreamTag
            name: gitlab-hugo-builder:latest
      triggers:
        - type: ConfigChange
        - type: ImageChange
          imageChange: {}
        - type: Generic
          generic:
            secret: ${GENERIC_WEBHOOK_SECRET}
        
  
  - kind: DeploymentConfig
    apiVersion: v1
    metadata:
      labels:
        app: hugo-cern
      name: hugo-builder
      annotations:
        template.alpha.openshift.io/wait-for-ready: "true"
    spec:
      replicas: 1
      selector:
        app: hugo-cern
        deploymentconfig: hugo-builder
      strategy:
        type: Rolling
      template:
        metadata:
          labels:
            app: hugo-cern
            deploymentconfig: hugo-builder
        spec:
          containers:
          - image: 'hugo-builder:latest'
            imagePullPolicy: Always
            name: hugo-app
            env:
              - name: BASE_URL
                value: ${BASE_URL}
            readinessProbe:
              failureThreshold: 3
              httpGet:
                path: /
                port: 1313
                scheme: HTTP
              initialDelaySeconds: 5
              periodSeconds: 10
              successThreshold: 1
              timeoutSeconds: 10
            livenessProbe:
              failureThreshold: 3
              httpGet:
                path: /
                port: 1313
                scheme: HTTP
              initialDelaySeconds: 900
              periodSeconds: 10
              successThreshold: 1
              timeoutSeconds: 10
            resources:
              limits:
                memory: 250Mi
                cpu: 50m
              requests:
                memory: 100Mi
                cpu: 25m
            terminationMessagePath: /dev/termination-log
            volumeMounts:
              - mountPath: /opt/app-root/src/resources
                name: resources
          dnsPolicy: ClusterFirst
          restartPolicy: Always
          securityContext: {}
          volumes:
            - emptyDir: {}
              name: resources
      test: false
      triggers:
        - type: ConfigChange
        - type: ImageChange
          imageChangeParams:
            automatic: true
            containerNames:
              - hugo-app
            from:
              kind: ImageStreamTag
              name: hugo-builder:latest            

  - kind: ImageStream
    apiVersion: v1  
    metadata:
      labels:
        app: hugo-cern
      name: gitlab-hugo-builder
    spec:
      tags:
      - annotations:
          openshift.io/display-name: HUGO
          sampleRepo: https://gitlab.cern.ch/webservices/hugo/hugo-scaffold-project.git
          tags: builder,hugo
        from:
          kind: DockerImage
          name: gitlab-registry.cern.ch/webservices/hugo/hugo-builder:v0.1
        importPolicy: {}
        name: "0.1"
        referencePolicy:
          type: Source
      - annotations:
          openshift.io/display-name: HUGO
          sampleRepo: https://gitlab.cern.ch/webservices/hugo/hugo-scaffold-project.git
          tags: builder,hugo
        from:
          kind: DockerImage
          name: gitlab-registry.cern.ch/webservices/hugo/hugo-builder:latest
        importPolicy: {}
        name: "latest"
        referencePolicy:
          type: Source

  - kind: ImageStream
    apiVersion: v1
    metadata:
      labels:
        app: hugo-cern
      name: hugo-builder
    spec: {}
    
parameters:
  - name: SOURCE_REPOSITORY_URL 
    displayName: Source Repository URL 
    description: The URL of the repository with your application source code.
    value: https://gitlab.cern.ch/my/repo.git
    required: true

  - name: SOURCE_REPOSITORY_REF 
    displayName: Source Repository REF 
    description: The branch of the repository where to point at (e.g., master, dev, etc.).
    value: master 
    required: true

  - name: GENERIC_WEBHOOK_SECRET
    displayName: Generic Webhook Secret.
    description: A secret string used to configure the Github/GitLab webhook.
    generate: expression 
    from: "[a-zA-Z0-9]{40}"
  
  - name: BASE_URL
    displayName: Base URL of your site.
    description: The URL your site will be deployed at (e.g., https://my-hugo-site.web.cern.ch).
    value: https://<my-site>.web.cern.ch
    required: true
message: "... The Generic webhook secret is ${GENERIC_WEBHOOK_SECRET} ..." 