FROM golang:1.14-alpine

LABEL io.openshift.s2i.scripts-url=image:///usr/libexec/s2i \
      io.k8s.description="Hugo Builder" \
      io.openshift.expose-services="1313:http" \
      io.k8s.display-name="Hugo Builder" \
      io.openshift.tags="builder,hugo" \
      maintainer="Web Services <web-services@cern.ch>"

RUN apk add hugo && \
    mkdir -p /opt/app-root/src/ && \
    chmod -R ug+rwx /opt/app-root && \
    chown -R 1001:0 /opt/app-root && \
    mkdir -p /usr/libexec/s2i

WORKDIR /opt/app-root/src

COPY ./s2i/bin/* /usr/libexec/s2i/
RUN chmod -R +x /usr/libexec/s2i/

USER 1001

CMD ["/usr/libexec/s2i/usage"]
