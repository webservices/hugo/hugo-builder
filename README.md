:warning: **This template is not officially supported. Use it under your responsibility.**

# HUGO for CERN template

This is a template intended to deploy static sites based on HUGO framework.

## Documentation

Go to [https://hugo-docs.web.cern.ch](https://hugo-docs.web.cern.ch) for further info.

## Deployment

### oc cli

```bash
oc project myproject

oc process -f template/hugo-cern.yml -p SOURCE_REPOSITORY_URL='https://gitlab.cern.ch/my/repo.git' -p SOURCE_REPOSITORY_REF='master' -p BASE_URL='https://my-site.web.cern.ch' | oc create -f -
```

### web UI

Just copy the content from `hugo-cern.yml`, and import it (through Import YAML) into the Openshift project.
